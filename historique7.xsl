<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" >
  <xsl:template match="/">
    <html>
      <head>
      </head>
      <body>
        <h2>Indépendants</h2>
        <ul>
          <xsl:apply-templates select="historique/promotion/etudiant/fonction[@type='independant']" />
        </ul>
        <h2>Salariés</h2>
        <ul>
          <xsl:apply-templates select="historique/promotion/etudiant/fonction[@type='salarie']" />
        </ul>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="etudiant[@sexe='h']/fonction">
    <li style="color: blue"><xsl:value-of select="../nom" /> : <xsl:value-of select="../../@annee" /></li>
  </xsl:template>

  <xsl:template match="etudiant[@sexe='f']/fonction">
    <li style="color: red"><xsl:value-of select="../nom" /> : <xsl:value-of select="../../@annee" /></li>
  </xsl:template>

</xsl:stylesheet>
