<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" >
  <xsl:template match="/">
    <html>
      <head>
        <style>
          td {
            border: 1px solid grey;
          }

          th {
            background-color: #333333;
            color: white;
            text-align: center;
          }
        </style>
      </head>
      <body>
        <h1>Historique <xsl:value-of select="historique/intitule" /></h1>
        <xsl:apply-templates select="historique/promotion" />
      </body>
    </html>
  </xsl:template>

  <xsl:template match="promotion">
    <h2>promotion <xsl:value-of select="@annee" /></h2>
    <table>
      <thead>
        <th></th>
        <th>salaries</th>
        <th>independants</th>
      </thead>
      <tbody>
        <tr>
          <th>filles</th>
          <td>
            <ol>
              <xsl:apply-templates select="etudiant[@sexe='f']/fonction[@type='salarie']" />
            </ol>
          </td>
          <td>
            <ol>
              <xsl:apply-templates select="etudiant[@sexe='f']/fonction[@type='independant']" />
            </ol>
          </td>
        </tr>
        <tr>
          <th>gars</th>
          <td>
            <ol>
              <xsl:apply-templates select="etudiant[@sexe='h']/fonction[@type='salarie']" />
            </ol>
          </td>
          <td>
            <ol>
              <xsl:apply-templates select="etudiant[@sexe='h']/fonction[@type='independant']" />
            </ol>
          </td>
        </tr>
      </tbody>
    </table>
  </xsl:template>

  <xsl:template match="etudiant/fonction">
    <li><xsl:value-of select="../nom" /> : <xsl:value-of select="." /></li>
  </xsl:template>

</xsl:stylesheet>
