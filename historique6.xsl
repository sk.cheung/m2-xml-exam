<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" >
  <xsl:template match="/">
    <html>
      <head>
        <style>
          thead th {
            text-align: center;
            font-weight: bold;
            background-color: #333333;
            color: white;
          }
          td {
            border: 1px solid grey;
            padding-left: 5px;
            padding-right: 5px;
          }
        </style>
      </head>
      <body>
        <h1><xsl:value-of select="count(historique/promotion/etudiant[@sexe='f'])" /> filles</h1>
        <table>
          <thead>
            <th>nom</th>
            <th>mention</th>
            <th>annee</th>
          </thead>
          <tbody>
            <xsl:apply-templates select="historique/promotion/etudiant[@sexe='f']" />
          </tbody>
        </table>
        <h1><xsl:value-of select="count(historique/promotion/etudiant[@sexe='h'])" /> gars</h1>
        <table>
          <thead>
            <th>nom</th>
            <th>mention</th>
            <th>annee</th>
          </thead>
          <tbody>
            <xsl:apply-templates select="historique/promotion/etudiant[@sexe='h']" />
          </tbody>
        </table>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="etudiant">
    <tr>
      <td><xsl:value-of select="nom" /></td>
      <td><xsl:value-of select="mention" /></td>
      <td><xsl:value-of select="../@annee" /></td>
    </tr>
  </xsl:template>

</xsl:stylesheet>
