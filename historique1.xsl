<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" >
  <xsl:template match="/">
    <html>
      <head>
      </head>
      <body>
        <xsl:apply-templates select="historique/intitule" />
        <xsl:apply-templates select="historique/promotion" />
      </body>
    </html>
  </xsl:template>

  <xsl:template match="intitule">
    <h1>Historique <xsl:value-of select="." /></h1>
  </xsl:template>

  <xsl:template match="promotion">
    <h2>Promotion <xsl:value-of select="@annee" /></h2>
  </xsl:template>
</xsl:stylesheet>
